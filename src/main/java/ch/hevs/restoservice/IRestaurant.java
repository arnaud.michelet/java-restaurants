package ch.hevs.restoservice;

import java.util.List;
import javax.ejb.Local;
import ch.hevs.businessobject.Menu;
import ch.hevs.businessobject.Plate;
import ch.hevs.businessobject.Rating;
import ch.hevs.businessobject.Restaurant;
import ch.hevs.businessobject.User;
import ch.hevs.exception.RestoException;

/**
 * Interface locale pour le service de restaurant.
 */
@Local
public interface IRestaurant {

	/**
	 * Récupère l'utilisateur connecté.
	 * 
	 * @return l'utilisateur connecté.
	 * @throws RestoException si une erreur survient lors de la récupération de
	 *                        l'utilisateur connecté.
	 */
	User getConnectedUser() throws RestoException;

	/**
	 * Vérifie si l'utilisateur connecté est propriétaire.
	 * 
	 * @return vrai si l'utilisateur connecté est propriétaire, faux sinon.
	 */
	boolean isOwner();

	/**
	 * Récupère la liste de tous les restaurants.
	 * 
	 * @return une liste de restaurants.
	 */
	List<Restaurant> getRestaurants();

	/**
	 * Récupère un restaurant par son identifiant.
	 * 
	 * @param restaurantId l'identifiant du restaurant.
	 * @return le restaurant correspondant à l'identifiant donné.
	 * @throws RestoException si une erreur survient lors de la récupération du
	 *                        restaurant.
	 */
	Restaurant getRestaurant(long restaurantId) throws RestoException;

	/**
	 * Récupère un menu par son identifiant.
	 * 
	 * @param menuId l'identifiant du menu.
	 * @return le menu correspondant à l'identifiant donné.
	 */
	Menu getMenu(long menuId);

	/**
	 * Récupère la liste de tous les menus.
	 * 
	 * @return une liste de menus.
	 */
	List<Menu> getMenus();

	/**
	 * Récupère la liste des menus d'un restaurant donné.
	 * 
	 * @param restaurant le restaurant dont on veut récupérer les menus.
	 * @return une liste de menus appartenant au restaurant donné.
	 */
	List<Menu> getMenusFromRestaurant(Restaurant restaurant);

	/**
	 * Met à jour un menu.
	 * 
	 * @param menu le menu à mettre à jour.
	 * @return un message indiquant le résultat de l'opération.
	 */
	String updateMenu(Menu menu);

	/**
	 * Supprime un menu.
	 * 
	 * @param menu le menu à supprimer.
	 * @return un message indiquant le résultat de l'opération.
	 */
	String deleteMenu(Menu menu);

	/**
	 * Récupère un plat par son identifiant.
	 * 
	 * @param plateId l'identifiant du plat.
	 * @return le plat correspondant à l'identifiant donné.
	 */
	Plate getPlate(long plateId);

	/**
	 * Récupère la liste des plats d'un menu donné.
	 * 
	 * @param menu le menu dont on veut récupérer les plats.
	 * @return une liste de plats appartenant au menu donné.
	 */
	List<Plate> getPlatesFromMenu(Menu menu);

	/**
	 * Récupère la liste de tous les plats.
	 * 
	 * @return une liste de plats.
	 */
	List<Plate> getPlates();

	/**
	 * Insère un nouveau plat.
	 * 
	 * @param plate le plat à insérer.
	 * @return un message indiquant le résultat de l'opération.
	 */
	String insertPlate(Plate plate);

	/**
	 * Met à jour un plat.
	 * 
	 * @param plate le plat à mettre à jour.
	 * @return un message indiquant le résultat de l'opération.
	 */
	String updatePlate(Plate plate);

	/**
	 * Supprime un plat.
	 * 
	 * @param plate le plat à supprimer.
	 * @return un message indiquant le résultat de l'opération.
	 */
	String deletePlate(Plate plate);

	/**
	 * Récupère une note par son identifiant.
	 * 
	 * @param ratingId l'identifiant de la note.
	 * @return la note correspondant à l'identifiant donné.
	 */
	Rating getRating(long ratingId);

	/**
	 * Récupère la liste des notes pour un restaurant donné.
	 * 
	 * @param restaurant le restaurant dont on veut récupérer les notes.
	 * @return une liste de notes pour le restaurant donné.
	 */
	List<Rating> getRatingsByRestaurant(Restaurant restaurant);

	/**
	 * Récupère la liste des notes d'un utilisateur donné.
	 * 
	 * @param customer l'utilisateur dont on veut récupérer les notes.
	 * @return une liste de notes de l'utilisateur donné.
	 */
	List<Rating> getRatingsByUser(User customer);

	/**
	 * Insère une nouvelle note.
	 * 
	 * @param rating la note à insérer.
	 * @return un message indiquant le résultat de l'opération.
	 */
	String insertRating(Rating rating);

	/**
	 * Met à jour une note.
	 * 
	 * @param rating  la note à mettre à jour.
	 * @param grade   la note sous forme de texte.
	 * @param comment le commentaire de la note.
	 * @return un message indiquant le résultat de l'opération.
	 */
	String updateRating(Rating rating, String grade, String comment);

	/**
	 * Supprime une note.
	 * 
	 * @param rating la note à supprimer.
	 * @return un message indiquant le résultat de l'opération.
	 */
	String deleteRating(Rating rating);

	/**
	 * Remplit la base de données avec des données initiales.
	 */
	void populate();
}
