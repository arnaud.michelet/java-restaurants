package ch.hevs.restoservice;

import java.util.List;

import javax.annotation.Resource;
import javax.annotation.security.RolesAllowed;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import ch.hevs.businessobject.Restaurant;
import ch.hevs.businessobject.Menu;
import ch.hevs.businessobject.Plate;
import ch.hevs.businessobject.Rating;
import ch.hevs.businessobject.User;
import ch.hevs.exception.RestoException;

/**
 * La classe RestoBean est utilisée pour interagir avec la base de données d'un
 * service de restauration. Elle implémente l'interface IRestaurant qui définit
 * les méthodes nécessaires pour gérer des restaurants, menus, plats, notes et
 * utilisateurs.
 * <p>
 * Cette classe est annotée en tant qu'EJB sans état (Stateless), ce qui
 * signifie qu'aucun état n'est conservé entre les appels de méthodes.
 *
 * @author Stateless
 * @see IRestaurant
 */
@Stateless
@RolesAllowed(value = { "owner", "customer" })
public class RestoBean implements IRestaurant {

	/**
	 * L'EntityManager est utilisé pour créer, lire, mettre à jour et supprimer des
	 * opérations sur des entités.
	 */
	@PersistenceContext(name = "RestoPU", type = PersistenceContextType.TRANSACTION)
	private EntityManager em;

	/**
	 * Le contexte de session fournit des informations sur l'utilisateur
	 * actuellement authentifié et son rôle.
	 */
	@Resource
	private SessionContext sctx;

	/**
	 * Enumération pour les opérations de base, utilisée dans les messages de
	 * réussite/échec.
	 */
	private enum Operation {
		INSERT, UPDATE, DELETE
	}

	/**
	 * Récupère l'utilisateur connecté.
	 *
	 * @return L'utilisateur connecté.
	 * @throws RestoException si l'utilisateur n'est pas trouvé.
	 */
	@Override
	public User getConnectedUser() {
		List<User> Users = em.createQuery("FROM User c", User.class).getResultList();
		if (Users.size() == 0) {
			populate();
		}
		try {
			User c = em.createQuery("FROM User c WHERE c.email=:email", User.class)
					.setParameter("email", sctx.getCallerPrincipal().getName()).getSingleResult();
			c.setRatings(em.createQuery("FROM Rating r WHERE r.user =: user", Rating.class).setParameter("user", c)
					.getResultList());
			return c;
		} catch (Exception e) {
			throw new RestoException("Viewer not found.", e);
		}
	}

	/**
	 * Construit un message de réussite pour une opération donnée.
	 *
	 * @param operation L'opération effectuée.
	 * @param val       La valeur sur laquelle l'opération a été effectuée.
	 * @return Le message de réussite.
	 */
	private String getSuccessMessage(Operation operation, String val) {
		return operation.toString() + " success for object '" + val + "'.";
	}

	/**
	 * Construit un message d'échec pour une opération donnée.
	 *
	 * @param operation L'opération qui a échoué.
	 * @return Le message d'échec.
	 */
	private String getFailureMessage(Operation operation) {
		return operation.toString() + " failed: " + sctx.getCallerPrincipal().getName() + " is not owner.";
	}

	/**
	 * Construit un message d'erreur pour une opération donnée.
	 *
	 * @param operation L'opération qui a causé l'erreur.
	 * @param e         L'exception déclenchée.
	 * @return Le message d'erreur.
	 */
	private String getErrorMessage(Operation operation, Exception e) {
		return operation.toString() + " error: " + e.getMessage();
	}

	/**
	 * Construit un message d'erreur pour une opération donnée.
	 *
	 * @param operation L'opération qui a causé l'erreur.
	 * @param message   Le message d'erreur.
	 * @return Le message d'erreur.
	 */
	private String getErrorMessage(Operation operation, String message) {
		return operation.toString() + " error: " + message;
	}

	/**
	 * Vérifie si l'utilisateur connecté est propriétaire.
	 *
	 * @return true si l'utilisateur est propriétaire, sinon false.
	 */
	@Override
	public boolean isOwner() {
		if (!sctx.isCallerInRole("owner")) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Récupère la liste de tous les restaurants.
	 *
	 * @return La liste des restaurants.
	 */
	@Override
	public List<Restaurant> getRestaurants() {
		List<Restaurant> restaurants = em.createQuery("FROM Restaurant r ORDER BY r.name", Restaurant.class)
				.getResultList();
		for (Restaurant restaurant : restaurants) {
			restaurant.setMenus(getMenusFromRestaurant(restaurant));
		}
		for (Restaurant restaurant : restaurants) {
			restaurant.setRatings(em.createQuery("FROM Rating r WHERE r.restaurant =: restaurant", Rating.class)
					.setParameter("restaurant", restaurant).getResultList());
		}
		return restaurants;
	}

	/**
	 * Récupère un restaurant par son identifiant.
	 *
	 * @param restaurantId L'identifiant du restaurant à récupérer.
	 * @return Le restaurant correspondant à l'identifiant donné.
	 * @throws RestoException si le restaurant n'est pas trouvé.
	 */
	@Override
	public Restaurant getRestaurant(long restaurantId) {
		try {
			Restaurant restaurant = em.find(Restaurant.class, restaurantId);
			restaurant.setRatings(em.createQuery("FROM Rating r WHERE r.restaurant =: restaurant", Rating.class)
					.setParameter("restaurant", restaurant).getResultList());
			return restaurant;
		} catch (Exception e) {
			throw new RestoException("Restaurant not found", e);
		}
	}

	/**
	 * Récupère la liste de tous les menus.
	 *
	 * @return La liste des menus.
	 */
	@Override
	public List<Menu> getMenus() {
		return em.createQuery("FROM Menu m ORDER BY m.name", Menu.class).getResultList();
	}

	/**
	 * Récupère la liste des menus d'un restaurant donné.
	 *
	 * @param restaurant Le restaurant dont on veut récupérer les menus.
	 * @return La liste des menus du restaurant donné.
	 */
	@Override
	public List<Menu> getMenusFromRestaurant(Restaurant restaurant) {
		return em.createQuery("SELECT m FROM Menu m WHERE m.restaurant = :restaurant", Menu.class)
				.setParameter("restaurant", restaurant).getResultList();
	}

	/**
	 * Supprime un menu.
	 *
	 * @param menu Le menu à supprimer.
	 * @return Un message indiquant si la suppression a réussi ou non.
	 */
	@Override
	public String deleteMenu(Menu menu) {
		if (menu == null) {
			return getErrorMessage(Operation.DELETE, "An existing menu must be chosen to delete it.");
		}
		String rv = getSuccessMessage(Operation.DELETE, menu.getName());
		try {
			// menu.getRestaurant().removeMenu(menu);
			em.remove(em.find(Menu.class, menu.getId()));
			em.flush();
			if (!sctx.isCallerInRole("owner")) {
				rv = getFailureMessage(Operation.DELETE);
				sctx.setRollbackOnly();
			}
		} catch (Exception e) {
			rv = getErrorMessage(Operation.DELETE, e);
		}
		return rv;
	}

	/**
	 * Supprime un plat.
	 *
	 * @param plate Le plat à supprimer.
	 * @return Un message indiquant si la suppression a réussi ou non.
	 */
	@Override
	public String deletePlate(Plate plate) {
		if (plate == null) {
			return getErrorMessage(Operation.DELETE, "An existing plate must be chosen to delete it.");
		}
		String rv = getSuccessMessage(Operation.DELETE, plate.getName());
		try {
			em.remove(em.find(Plate.class, plate.getId()));
			em.flush();
			if (!sctx.isCallerInRole("owner")) {
				rv = getFailureMessage(Operation.DELETE);
				sctx.setRollbackOnly();
			}
		} catch (Exception e) {
			rv = getErrorMessage(Operation.DELETE, e);
		}
		return rv;
	}

	/**
	 * Récupère la liste des plats d'un menu donné.
	 *
	 * @param menu Le menu dont on veut récupérer les plats.
	 * @return La liste des plats du menu donné.
	 */
	@Override
	public List<Plate> getPlatesFromMenu(Menu menu) {
		return em.createQuery("SELECT p FROM Plate p WHERE p.menu = :menu", Plate.class).setParameter("menu", menu)
				.getResultList();
	}

	/**
	 * Récupère une note par son identifiant.
	 *
	 * @param ratingId L'identifiant de la note à récupérer.
	 * @return La note correspondant à l'identifiant donné.
	 * @throws RestoException si la note n'est pas trouvée.
	 */
	@Override
	public Rating getRating(long ratingId) {
		try {
			return em.find(Rating.class, ratingId);
		} catch (Exception e) {
			throw new RestoException("Rating not found", e);
		}
	}

	/**
	 * Récupère la liste des évaluations pour un restaurant donné.
	 *
	 * @param restaurant Le restaurant pour lequel récupérer les évaluations.
	 * @return La liste des évaluations associées au restaurant.
	 */
	@Override
	public List<Rating> getRatingsByRestaurant(Restaurant restaurant) {
		return em.createQuery("SELECT r FROM Rating r WHERE r.restaurant = :restaurant", Rating.class)
				.setParameter("restaurant", restaurant).getResultList();
	}

	/**
	 * Récupère la liste des évaluations effectuées par un utilisateur donné.
	 *
	 * @param user L'utilisateur pour lequel récupérer les évaluations.
	 * @return La liste des évaluations associées à l'utilisateur.
	 */
	@Override
	public List<Rating> getRatingsByUser(User User) {
		return em.createQuery("SELECT r FROM Rating r WHERE r.User = :User", Rating.class).setParameter("User", User)
				.getResultList();
	}

	/**
	 * Insère une nouvelle évaluation.
	 *
	 * @param rating L'évaluation à insérer.
	 * @return Le message de réussite ou d'échec de l'opération.
	 */
	@Override
	public String insertRating(Rating rating) {
		if (rating.getRestaurant() == null || rating.getUser() == null) {
			return getErrorMessage(Operation.INSERT, "A rating must be linked to a restaurant and a User.");
		}
		String rv = getSuccessMessage(Operation.INSERT, String.valueOf(rating.getComment()));
		try {
			rating.getRestaurant().addRating(rating);
			rating.getUser().addRating(rating);
			em.persist(rating);
			em.flush();
			if (!sctx.isCallerInRole("customer")) {
				rv = getFailureMessage(Operation.INSERT);
				sctx.setRollbackOnly();
			}
		} catch (Exception e) {
			rv = getErrorMessage(Operation.INSERT, e);
		}
		return rv;
	}

	/**
	 * Insère un nouveau plat.
	 *
	 * @param plate Le plat à insérer.
	 * @return Le message de réussite ou d'échec de l'opération.
	 */
	@Override
	public String insertPlate(Plate plate) {
		if (plate.getMenu() == null) {
			return getErrorMessage(Operation.INSERT, "A plate must be linked to a menu.");
		}
		String rv = getSuccessMessage(Operation.INSERT, plate.getName());
		try {
			plate.getMenu().addPlate(plate);
			em.persist(plate);
			em.flush();
			if (!sctx.isCallerInRole("owner")) {
				rv = getFailureMessage(Operation.INSERT);
				sctx.setRollbackOnly();
			}
		} catch (Exception e) {
			rv = getErrorMessage(Operation.INSERT, e);
		}
		return rv;
	}

	/**
	 * Met à jour un menu existant.
	 *
	 * @param menu Le menu à mettre à jour.
	 * @return Le message de réussite ou d'échec de l'opération.
	 */
	@Override
	public String updateMenu(Menu menu) {
		if (menu == null) {
			return getErrorMessage(Operation.UPDATE, "An existing menu must be chosen to update it.");
		}
		String rv = getSuccessMessage(Operation.UPDATE, menu.getName());
		try {
			em.merge(menu);
			em.flush();
			if (!sctx.isCallerInRole("owner")) {
				rv = getFailureMessage(Operation.UPDATE);
				sctx.setRollbackOnly();
			}
		} catch (Exception e) {
			rv = getErrorMessage(Operation.UPDATE, e);
		}
		return rv;
	}

	/**
	 * Met à jour un plat existant.
	 *
	 * @param plate Le plat à mettre à jour.
	 * @return Le message de réussite ou d'échec de l'opération.
	 */
	@Override
	public String updatePlate(Plate plate) {
		if (plate == null) {
			return getErrorMessage(Operation.UPDATE, "An existing dish must be chosen to update it.");
		}
		String rv = getSuccessMessage(Operation.UPDATE, plate.getName());
		try {
			em.merge(plate);
			em.flush();
			if (!sctx.isCallerInRole("owner")) {
				rv = getFailureMessage(Operation.UPDATE);
				sctx.setRollbackOnly();
			}
		} catch (Exception e) {
			rv = getErrorMessage(Operation.UPDATE, e);
		}
		return rv;
	}

	/**
	 * Met à jour une évaluation existante.
	 *
	 * @param rating  L'évaluation à mettre à jour.
	 * @param grade   La note attribuée à l'évaluation.
	 * @param comment Le commentaire associé à l'évaluation.
	 * @return Le message de réussite ou d'échec de l'opération.
	 */
	@Override
	public String updateRating(Rating rating, String grade, String comment) {
		if (rating == null) {
			return getErrorMessage(Operation.UPDATE, "An existing rating must be chosen to update it.");
		}
		rating.setGrade(grade);

		String rv = getSuccessMessage(Operation.UPDATE, rating.getComment());
		try {
			em.merge(rating);
			em.flush();
			if (!sctx.isCallerInRole("User")) {
				rv = getFailureMessage(Operation.UPDATE);
				sctx.setRollbackOnly();
			}
		} catch (Exception e) {
			rv = getErrorMessage(Operation.UPDATE, e);
		}
		return rv;
	}

	/**
	 * Supprime une évaluation existante.
	 *
	 * @param rating L'évaluation à supprimer.
	 * @return Le message de réussite ou d'échec de l'opération.
	 */
	@Override
	public String deleteRating(Rating rating) {
		if (rating == null) {
			return getErrorMessage(Operation.DELETE, "An existing rating must be chosen to delete it.");
		}
		String rv = getSuccessMessage(Operation.DELETE, rating.getComment());
		try {
			rating.getRestaurant().removeRating(rating);
			rating.getUser().removeRating(rating);
			em.remove(em.find(Rating.class, rating.getId()));
			em.flush();
			if (!sctx.isCallerInRole("owner")) {
				rv = getFailureMessage(Operation.DELETE);
				sctx.setRollbackOnly();
			}
		} catch (Exception e) {
			rv = getErrorMessage(Operation.DELETE, e);
		}
		return rv;
	}

	/**
	 * Popule la base de données avec des données de test.
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void populate() {
		Long userCount = em.createQuery("SELECT COUNT(u) FROM User u", Long.class).getSingleResult();

		if (userCount == 0) {
			try {
				Restaurant restaurant1 = new Restaurant("A la bonne bidoche", "Rue de la soif 69 - 1994 Aproz",
						"0273462030");
				Restaurant restaurant2 = new Restaurant("VeganLand", "Rue du gerbi 4 - 1005 Lausanne", "0277771299");

				Menu menu1 = new Menu("LectureTartare", restaurant1);
				Menu menu2 = new Menu("CourgetteRead", restaurant2);

				Plate plate1 = new Plate("La Princesse Rôtie", "Côte de boeuf pour 2 personnes", 120, menu1);
				Plate plate2 = new Plate("Tartare", "Boeuf, servi avec des frites", 35, menu1);
				Plate plate3 = new Plate("La Poulette", "Demi poulet rôti en sauce", 30, menu1);
				Plate plate4 = new Plate("Planchette Apéro", "Saucisson du pays, Viande Séchée, Jambon et Fromages", 40,
						menu1);
				Plate plate5 = new Plate("Marmotte", "Marmotte entière, attention aux petits os", 60, menu1);

				Plate plate6 = new Plate("Tofuchiasse", "Superbe pièce de non viande", 300, menu2);
				Plate plate7 = new Plate("Steack de Soja", "Vous aurez encore faim...", 100, menu2);
				Plate plate8 = new Plate("Pain sans farine", "Inspiré du célèbre cuisto Mike Horn", 50, menu2);
				Plate plate9 = new Plate("Bol d'air frais", "Particulièrement prisé des touristes chinois", 120, menu2);
				Plate plate10 = new Plate("Soupe d'argile", "Attention aux calories !", 9, menu2);

				// CUSTOMERS
				User user1 = new User("Pétole", "Baillifard", "petole@petole.ch");
				User user2 = new User("Pasune", "Jean-Plante", "pas@pas.ch");
				// OWNERS
				User user3 = new User("Jean", "Hairienafout", "jean@jean.ch");
				User user4 = new User("Paul", "Hayouk", "paul@paul.ch");

				Rating rating1 = new Rating("5 étoiles",
						"J'ai adoré, à recommander chaudement à tous les amateurs de viandes", restaurant1, user1);
				Rating rating2 = new Rating("5 étoiles", "Un bol d'air frais comme on ne voit plus", restaurant2,
						user2);

				em.persist(restaurant1);
				em.persist(restaurant2);

				em.persist(menu1);
				em.persist(menu2);

				em.persist(plate1);
				em.persist(plate2);
				em.persist(plate3);
				em.persist(plate4);
				em.persist(plate5);
				em.persist(plate6);
				em.persist(plate7);
				em.persist(plate8);
				em.persist(plate9);
				em.persist(plate10);

				em.persist(user1);
				em.persist(user2);
				em.persist(user3);
				em.persist(user4);

				em.persist(rating1);
				em.persist(rating2);

				em.flush();

			} catch (Exception e) {
				throw new RestoException("Populated failed !", e);
			}
		}
	}

	/**
	 * Récupère la liste des plats.
	 *
	 * @return La liste des plats.
	 */
	@Override
	public List<Plate> getPlates() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Récupère un menu en fonction de son identifiant.
	 *
	 * @param menuId L'identifiant du menu à récupérer.
	 * @return Le menu correspondant à l'identifiant.
	 */
	@Override
	public Menu getMenu(long menuId) {
		try {
			return em.find(Menu.class, menuId);
		} catch (Exception e) {
			throw new RestoException("Menu not found", e);
		}
	}

	/**
	 * Récupère un plat en fonction de son identifiant.
	 *
	 * @param plateId L'identifiant du plat à récupérer.
	 * @return Le plat correspondant à l'identifiant.
	 */
	@Override
	public Plate getPlate(long plateId) {
		try {
			return em.find(Plate.class, plateId);
		} catch (Exception e) {
			throw new RestoException("Plate not found", e);
		}
	}
}
