package ch.hevs.managedbeans;

import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import ch.hevs.businessobject.User;
import ch.hevs.businessobject.Menu;
import ch.hevs.businessobject.Plate;
import ch.hevs.businessobject.Rating;
import ch.hevs.businessobject.Restaurant;
import ch.hevs.restoservice.IRestaurant;

/**
 * RestaurantBean est une classe Java qui gère les interactions avec les objets
 * Restaurant de la couche présentation.
 */
public class RestaurantBean {

	private User customer;
	private List<Restaurant> restaurants;
	private List<Menu> menus;
	private IRestaurant iRestaurant;
	private Long menuEditingId;
	private Long plateEditingId;
	private String currentMenu;
	private String currentPlateDescription;
	private String currentPlateName;
	private HashMap<Long, String> currentRatingGrade;
	private HashMap<Long, String> currentRatingComment;
	private boolean isOwner;

	/**
	 * Retourne les notes courantes sous forme de HashMap.
	 * 
	 * @return HashMap contenant les notes.
	 */
	public HashMap<Long, String> getCurrentRatingGrade() {
		return currentRatingGrade;
	}

	/**
	 * Définit les notes courantes.
	 * 
	 * @param currentRatingGrade HashMap contenant les notes.
	 */
	public void setCurrentRatingGrade(HashMap<Long, String> currentRatingGrade) {
		this.currentRatingGrade = currentRatingGrade;
	}

	/**
	 * Retourne les commentaires de notes courants sous forme de HashMap.
	 * 
	 * @return HashMap contenant les commentaires de notes.
	 */
	public HashMap<Long, String> getCurrentRatingComment() {
		return currentRatingComment;
	}

	/**
	 * Définit les commentaires de notes courants.
	 * 
	 * @param currentRatingComment HashMap contenant les commentaires de notes.
	 */
	public void setCurrentRatingComment(HashMap<Long, String> currentRatingComment) {
		this.currentRatingComment = currentRatingComment;
	}

	/**
	 * Initialise le bean après sa création.
	 * 
	 * @throws NamingException En cas d'erreur lors de la recherche du service.
	 */
	@PostConstruct
	public void initialize() throws NamingException {
		InitialContext ctx = new InitialContext();
		iRestaurant = (IRestaurant) ctx
				.lookup("java:global/Resto-WEB-EJB-PC-EPC-E-0.0.1-SNAPSHOT/RestoBean!ch.hevs.restoservice.IRestaurant");
		iRestaurant.populate();
		isOwner = iRestaurant.isOwner();
		restaurants = iRestaurant.getRestaurants();
		menus = iRestaurant.getMenus();
		currentRatingGrade = new HashMap<>();
		currentRatingComment = new HashMap<>();
	}

	/**
	 * Récupère le menu courant.
	 * 
	 * @return le menu courant sous forme de chaîne de caractères.
	 */
	public String getCurrentMenu() {
		return currentMenu;
	}

	/**
	 * Définit le menu courant.
	 * 
	 * @param currentMenu le nom du menu à définir.
	 */
	public void setCurrentMenu(String currentMenu) {
		this.currentMenu = currentMenu;
	}

	/**
	 * Récupère la liste des restaurants.
	 * 
	 * @return une liste d'objets Restaurant.
	 */
	public List<Restaurant> getRestaurants() {
		return restaurants;
	}

	/**
	 * Définit l'identifiant du menu en cours de modification.
	 * 
	 * @param menuEditingId l'identifiant du menu en cours de modification.
	 */
	public void setMenuEditingId(Long menuEditingId) {
		this.menuEditingId = menuEditingId;
	}

	/**
	 * Récupère l'identifiant du menu en cours de modification.
	 * 
	 * @return l'identifiant du menu en cours de modification.
	 */
	public Long getMenuEditingId() {
		return menuEditingId;
	}

	/**
	 * Récupère l'utilisateur client.
	 * 
	 * @return un objet User représentant le client.
	 */
	public User getCustomer() {
		return customer;
	}

	/**
	 * Définit l'utilisateur client.
	 * 
	 * @param customer un objet User représentant le client.
	 */
	public void setCustomer(User customer) {
		this.customer = customer;
	}

	/**
	 * Récupère la liste des menus.
	 * 
	 * @return une liste d'objets Menu.
	 */
	public List<Menu> getMenus() {
		return menus;
	}

	/**
	 * Définit la liste des menus.
	 * 
	 * @param menus une liste d'objets Menu.
	 */
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	/**
	 * Indique si l'utilisateur est propriétaire.
	 * 
	 * @return vrai si l'utilisateur est propriétaire, faux sinon.
	 */
	public boolean getIsOwner() {
		return isOwner;
	}

	/**
	 * Récupère le service de restaurant.
	 * 
	 * @return un objet IRestaurant représentant le service de restaurant.
	 */
	public IRestaurant getiRestaurant() {
		return iRestaurant;
	}

	/**
	 * Définit le service de restaurant.
	 * 
	 * @param iRestaurant un objet IRestaurant représentant le service de
	 *                    restaurant.
	 */
	public void setiRestaurant(IRestaurant iRestaurant) {
		this.iRestaurant = iRestaurant;
	}

	/**
	 * Définit la liste des restaurants.
	 * 
	 * @param restaurants une liste d'objets Restaurant.
	 */
	public void setRestaurants(List<Restaurant> restaurants) {
		this.restaurants = restaurants;
	}

	/**
	 * Récupère l'identifiant du plat en cours de modification.
	 * 
	 * @return l'identifiant du plat en cours de modification.
	 */
	public Long getPlateEditingId() {
		return plateEditingId;
	}

	/**
	 * Définit l'identifiant du plat en cours de modification.
	 * 
	 * @param plateEditingId l'identifiant du plat en cours de modification.
	 */
	public void setPlateEditingId(Long plateEditingId) {
		this.plateEditingId = plateEditingId;
	}

	/**
	 * Récupère la description du plat courant.
	 * 
	 * @return la description du plat courant sous forme de chaîne de caractères.
	 */
	public String getCurrentPlateDescription() {
		return currentPlateDescription;
	}

	/**
	 * Définit la description du plat courant.
	 * 
	 * @param currentPlateDescription la description du plat à définir.
	 */
	public void setCurrentPlateDescription(String currentPlateDescription) {
		this.currentPlateDescription = currentPlateDescription;
	}

	/**
	 * Récupère le nom du plat courant.
	 * 
	 * @return le nom du plat courant sous forme de chaîne de caractères.
	 */
	public String getCurrentPlateName() {
		return currentPlateName;
	}

	/**
	 * Définit le nom du plat courant.
	 * 
	 * @param currentPlateName le nom du plat à définir.
	 */
	public void setCurrentPlateName(String currentPlateName) {
		this.currentPlateName = currentPlateName;
	}

	/**
	 * Récupère le nom de l'utilisateur connecté.
	 * 
	 * @return le nom de l'utilisateur connecté sous forme de chaîne de caractères.
	 */
	public String getConnectedUserName() {
		return iRestaurant.getConnectedUser().getName();
	}

	/**
	 * Modifie un menu existant ou passe en mode édition si l'identifiant de menu
	 * est reçu.
	 */
	public void modifyMenu() {
		Long menuId = Long.parseLong(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("menuId"));
		Menu menu = iRestaurant.getMenu(menuId);

		if (menuEditingId != null && menuEditingId.equals(menuId)) {
			menu.setName(currentMenu);
			iRestaurant.updateMenu(menu);
			restaurants = iRestaurant.getRestaurants();
			menuEditingId = null; // réinitialise l'état d'édition
		} else {
			currentMenu = menu.getName();
			menuEditingId = menuId; // passe en mode édition pour ce menu
		}
	}

	/**
	 * Supprime un menu existant.
	 */
	public void deleteMenu() {
		Long menuId = Long.parseLong(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("menuId"));
		Menu menu = iRestaurant.getMenu(menuId);
		for (Restaurant restaurant : restaurants) {
			restaurant.removeMenu(menu);
		}
		iRestaurant.deleteMenu(menu);
	}

	/**
	 * Modifie un plat existant ou passe en mode édition si l'identifiant de plat
	 * est reçu.
	 */
	public void modifyPlate() {
		Long plateId = Long.parseLong(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("plateId"));
		Plate plate = iRestaurant.getPlate(plateId);

		if (plateEditingId != null && plateEditingId.equals(plateId)) {
			plate.setName(currentPlateName);
			plate.setDescription(currentPlateDescription);
			iRestaurant.updatePlate(plate);
			menus = iRestaurant.getMenus();
			plateEditingId = null; // réinitialise l'état d'édition
		} else {
			currentPlateName = plate.getName();
			currentPlateDescription = plate.getDescription();
			plateEditingId = plateId; // passe en mode édition pour ce plat
		}
	}

	/**
	 * Supprime un plat existant.
	 */
	public void deletePlate() {
		Long plateId = Long.parseLong(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("plateId"));
		Plate plate = iRestaurant.getPlate(plateId);

		for (Menu menu : menus) {
			menu.removePlate(plate);
		}
		iRestaurant.deletePlate(plate);
	}

	/**
	 * Ajoute une note à un restaurant.
	 */
	public void addRating() {
		Long restaurantId = Long.parseLong(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("restaurantId"));
		Rating rating = new Rating(currentRatingGrade.get(restaurantId), currentRatingComment.get(restaurantId),
				iRestaurant.getRestaurant(restaurantId), iRestaurant.getConnectedUser());
		System.out.println(iRestaurant.insertRating(rating));
		restaurants = iRestaurant.getRestaurants();
	}
}