package ch.hevs.exception;

/**
 * Classe RestoException qui étend RuntimeException. Cette classe est utilisée pour gérer les exceptions spécifiques
 * liées aux fonctionnalités du restaurant.
 */
public class RestoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

    /**
     * Constructeur par défaut. Crée une nouvelle instance de RestoException sans message d'erreur détaillé.
     */
	public RestoException() {
		super();
	}

    /**
     * Constructeur permettant de spécifier un message d'erreur.
     *
     * @param arg0 Le message d'erreur détaillé utilisé pour cette exception.
     */
	public RestoException(String arg0) {
		super(arg0);
	}

    /**
     * Constructeur permettant de spécifier un message d'erreur et une cause.
     *
     * @param arg0 Le message d'erreur détaillé utilisé pour cette exception.
     * @param arg1 La cause de l'exception (généralement une autre exception).
     */
	public RestoException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

    /**
     * Constructeur permettant de spécifier une cause.
     *
     * @param arg0 La cause de l'exception (généralement une autre exception).
     */
	public RestoException(Throwable arg0) {
		super(arg0);
	}
}