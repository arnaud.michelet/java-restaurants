package ch.hevs.businessobject;

import javax.persistence.*;

/**
 * Représente un plat dans un menu de restaurant.
 *
 * @author [Arnaud Michelet & Arthur Avez]
 * @version 1.0
 */
@Entity
@Table(name = "Plate")
public class Plate {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "description")
	private String description;

	@Column(name = "Price")
	private double price;

	@ManyToOne
	@JoinColumn(name = "FK_MENU")
	private Menu menu;

	/**
	 * Constructeur par défaut.
	 */
	public Plate() {
		super();
	}

	/**
	 * Construit un plat avec un nom, une description, un prix et un menu associé.
	 *
	 * @param name        le nom du plat.
	 * @param description la description du plat.
	 * @param price       le prix du plat.
	 * @param menu        le menu auquel appartient le plat.
	 */
	public Plate(String name, String description, double price, Menu menu) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.menu = menu;
	}

	/**
	 * Retourne l'ID du plat.
	 *
	 * @return ID du plat.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Définit l'ID du plat.
	 *
	 * @param id l'ID à définir.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Retourne le nom du plat.
	 *
	 * @return le nom du plat.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Définit le nom du plat.
	 *
	 * @param name le nom à définir.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retourne la description du plat.
	 *
	 * @return la description du plat.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Définit la description du plat.
	 *
	 * @param description la description à définir.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Retourne le prix du plat.
	 *
	 * @return le prix du plat.
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Définit le prix du plat.
	 *
	 * @param price le prix à définir.
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Retourne le menu auquel ce plat appartient.
	 *
	 * @return le menu associé.
	 */
	public Menu getMenu() {
		return menu;
	}

	/**
	 * Définit le menu auquel ce plat appartient.
	 *
	 * @param menu le menu à associer.
	 */
	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	/**
	 * Retourne une représentation sous forme de chaîne de caractères de l'objet
	 * Plate.
	 *
	 * @return une chaîne de caractères représentant l'objet Plate.
	 */
	@Override
	public String toString() {
		return "Plate [id=" + id + ", name=" + name + ", description=" + description + ", price=" + price + ", menu="
				+ menu + "]";
	}

	/**
	 * Retourne le code de hachage de cet objet Plate, basé sur son ID.
	 *
	 * @return le code de hachage de cet objet Plate.
	 */
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	/**
	 * Détermine si un autre objet est égal à cet objet Plate. Les plats sont
	 * considérés comme égaux s'ils ont le même ID.
	 *
	 * @param obj l'objet à comparer avec cet objet Plate.
	 * @return vrai si les objets sont égaux, faux sinon.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Plate)) {
			return false;
		}
		Plate other = (Plate) obj;
		return other.id.equals(this.id);
	}
}
