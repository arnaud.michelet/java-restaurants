package ch.hevs.businessobject;

import javax.persistence.*;

/**
 * Représente un avis/rating donné par un utilisateur à un restaurant. Un avis
 * est composé d'une note et d'un commentaire.
 *
 * @author [Arnaud Michelet & Arthur Avez]
 * @version 1.0
 */
@Entity
@Table(name = "Rating")
public class Rating {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "grade")
	private String grade;

	@Column(name = "comment")
	private String comment;

	@ManyToOne
	@JoinColumn(name = "FK_USER")
	private User user;

	@ManyToOne
	@JoinColumn(name = "FK_RESTAURANT")
	private Restaurant restaurant;

	/**
	 * Constructeur par défaut.
	 */
	public Rating() {
		super();
	}

	/**
	 * Construit un avis avec une note, un commentaire, un restaurant et un
	 * utilisateur associés.
	 *
	 * @param grade      la note donnée au restaurant.
	 * @param comment    le commentaire laissé par l'utilisateur.
	 * @param restaurant le restaurant auquel l'avis est associé.
	 * @param user       l'utilisateur qui a laissé l'avis.
	 */
	public Rating(String grade, String comment, Restaurant restaurant, User user) {
		this.grade = grade;
		this.comment = comment;
		this.restaurant = restaurant;
		this.user = user;
	}

	/**
	 * Retourne l'ID de l'avis.
	 *
	 * @return l'ID de l'avis.
	 */
	public long getId() {
		return id;
	}

	/**
	 * Définit l'ID de l'avis.
	 *
	 * @param id l'ID à définir.
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Retourne la note de l'avis.
	 *
	 * @return la note de l'avis.
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * Définit la note de l'avis.
	 *
	 * @param grade la note à définir.
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * Retourne le commentaire de l'avis.
	 *
	 * @return le commentaire de l'avis.
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Définit le commentaire de l'avis.
	 *
	 * @param comment le commentaire à définir.
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Retourne l'utilisateur qui a laissé l'avis.
	 *
	 * @return l'utilisateur associé à l'avis.
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Définit l'utilisateur qui a laissé l'avis.
	 *
	 * @param user l'utilisateur à associer à l'avis.
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Retourne le restaurant auquel l'avis est associé.
	 *
	 * @return le restaurant associé à l'avis.
	 */
	public Restaurant getRestaurant() {
		return restaurant;
	}

	/**
	 * Définit le restaurant auquel l'avis est associé.
	 *
	 * @param restaurant le restaurant à associer à l'avis.
	 */
	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	/**
	 * Retourne le code de hachage de cet objet Rating, basé sur son ID.
	 *
	 * @return le code de hachage de cet objet Rating.
	 */
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	/**
	 * Détermine si un autre objet est égal à cet objet Rating. Les avis sont
	 * considérés comme égaux s'ils ont le même ID.
	 *
	 * @param obj l'objet à comparer avec cet objet Rating.
	 * @return vrai si les objets sont égaux, faux sinon.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Rating)) {
			return false;
		}
		Rating other = (Rating) obj;
		return other.id.equals(this.id);
	}
}
