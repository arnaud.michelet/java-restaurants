package ch.hevs.businessobject;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;

/**
 * Classe représentant un restaurant. Un restaurant a un identifiant unique, un
 * nom, une localisation, un numéro de téléphone, une liste de menus et une
 * liste d'avis.
 *
 * @author [Arnaud Michelet & Arthur Avez]
 */
@Entity
@Table(name = "Restaurant")
public class Restaurant {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "location")
	private String location;

	@Column(name = "phonenumber")
	private String phonenumber;

	@OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
	private List<Menu> menus;

	@OneToMany(mappedBy = "restaurant", cascade = CascadeType.ALL)
	private List<Rating> ratings;

	/**
	 * Retourne l'identifiant du restaurant.
	 *
	 * @return l'identifiant du restaurant.
	 */
	public long getId() {
		return id;
	}

	/**
	 * Définit l'identifiant du restaurant.
	 *
	 * @param id l'identifiant à affecter au restaurant.
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Retourne le nom du restaurant.
	 *
	 * @return le nom du restaurant.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Définit le nom du restaurant.
	 *
	 * @param name le nom à affecter au restaurant.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retourne la localisation du restaurant.
	 *
	 * @return la localisation du restaurant.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Définit la localisation du restaurant.
	 *
	 * @param location la localisation à affecter au restaurant.
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Retourne le numéro de téléphone du restaurant.
	 *
	 * @return le numéro de téléphone du restaurant.
	 */
	public String getPhonenumber() {
		return phonenumber;
	}

	/**
	 * Définit le numéro de téléphone du restaurant.
	 *
	 * @param phonenumber le numéro de téléphone à affecter au restaurant.
	 */
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	/**
	 * Retourne la liste des menus du restaurant.
	 *
	 * @return la liste des menus.
	 */
	public List<Menu> getMenus() {
		return menus;
	}

	/**
	 * Définit la liste des menus du restaurant.
	 *
	 * @param menus la liste des menus à affecter au restaurant.
	 */
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}

	/**
	 * Ajoute un menu à la liste des menus du restaurant.
	 *
	 * @param menu le menu à ajouter.
	 */
	public void addMenu(Menu menu) {
		menus.add(menu);
	}

	/**
	 * Supprime un menu de la liste des menus du restaurant.
	 *
	 * @param menu le menu à supprimer.
	 */
	public void removeMenu(Menu menu) {
		menus.remove(menu);
	}

	/**
	 * Retourne la liste des avis du restaurant.
	 *
	 * @return la liste des avis.
	 */
	public List<Rating> getRatings() {
		return ratings;
	}

	/**
	 * Définit la liste des avis du restaurant.
	 *
	 * @param ratings la liste des avis à affecter au restaurant.
	 */
	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	/**
	 * Ajoute un avis à la liste des avis du restaurant.
	 *
	 * @param rating l'avis à ajouter.
	 */
	public void addRating(Rating rating) {
		ratings.add(rating);
	}

	/**
	 * Supprime un avis de la liste des avis du restaurant.
	 *
	 * @param rating l'avis à supprimer.
	 */
	public void removeRating(Rating rating) {
		ratings.remove(rating);
	}

	/**
	 * Constructeur par défaut. Initialise les listes de menus et d'avis.
	 */
	public Restaurant() {
		super();
		ratings = new ArrayList<Rating>();
		menus = new ArrayList<Menu>();
	}

	/**
	 * Constructeur avec paramètres. Initialise les listes de menus et d'avis, et
	 * affecte les valeurs des paramètres aux attributs correspondants.
	 *
	 * @param name        le nom du restaurant.
	 * @param location    la localisation du restaurant.
	 * @param phonenumber le numéro de téléphone du restaurant.
	 */
	public Restaurant(String name, String location, String phonenumber) {
		this.name = name;
		this.location = location;
		this.phonenumber = phonenumber;
		ratings = new ArrayList<Rating>();
		menus = new ArrayList<Menu>();
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Restaurant)) {
			return false;
		}
		Restaurant other = (Restaurant) obj;
		return other.id.equals(this.id);
	}
}
