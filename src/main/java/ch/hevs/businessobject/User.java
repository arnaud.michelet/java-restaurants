package ch.hevs.businessobject;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.ArrayList;

/**
 * Entité représentant un utilisateur dans le système.
 *
 * Cette classe représente un utilisateur, avec son nom, prénom, et email. Elle
 * contient également la liste des avis donnés par l'utilisateur.
 *
 * @author [Arnaud Michelet & Arthur Avez]
 */
@Entity
@Table(name = "User")
public class User {

	/**
	 * Identifiant unique de l'utilisateur.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	/**
	 * Nom de l'utilisateur.
	 */
	@Column(name = "name")
	private String name;

	/**
	 * Prénom de l'utilisateur.
	 */
	@Column(name = "surname")
	private String surname;

	/**
	 * Email de l'utilisateur.
	 */
	@Column(name = "email")
	private String email;

	/**
	 * Liste des avis donnés par l'utilisateur.
	 */
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<Rating> ratings;

	/**
	 * Récupère l'identifiant de l'utilisateur.
	 *
	 * @return l'identifiant de l'utilisateur.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Définit l'identifiant de l'utilisateur.
	 *
	 * @param id l'identifiant à affecter à l'utilisateur.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Récupère le nom de l'utilisateur.
	 *
	 * @return le nom de l'utilisateur.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Définit le nom de l'utilisateur.
	 *
	 * @param name le nom à affecter à l'utilisateur.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Récupère le prénom de l'utilisateur.
	 *
	 * @return le prénom de l'utilisateur.
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Définit le prénom de l'utilisateur.
	 *
	 * @param surname le prénom à affecter à l'utilisateur.
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * Récupère l'email de l'utilisateur.
	 *
	 * @return l'email de l'utilisateur.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Définit l'email de l'utilisateur.
	 *
	 * @param email l'email à affecter à l'utilisateur.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Récupère la liste des avis de l'utilisateur.
	 *
	 * @return la liste des avis.
	 */
	public List<Rating> getRatings() {
		return ratings;
	}

	/**
	 * Définit la liste des avis de l'utilisateur.
	 *
	 * @param ratings la liste des avis à affecter à l'utilisateur.
	 */
	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	/**
	 * Supprime un avis de la liste des avis de l'utilisateur.
	 *
	 * @param rating l'avis à supprimer.
	 */
	public void removeRating(Rating rating) {
		ratings.remove(rating);
	}

	/**
	 * Constructeur par défaut. Initialise la liste des avis.
	 */
	public User() {
		ratings = new ArrayList<Rating>();
	}

	/**
	 * Ajoute un avis à la liste des avis de l'utilisateur.
	 *
	 * @param rating l'avis à ajouter.
	 */
	public void addRating(Rating rating) {
		ratings.add(rating);
	}

	/**
	 * Constructeur avec paramètres.
	 *
	 * @param name    le nom de l'utilisateur.
	 * @param surname le prénom de l'utilisateur.
	 * @param email   l'email de l'utilisateur.
	 */
	public User(String name, String surname, String email) {
		this.name = name;
		this.surname = surname;
		this.email = email;
		ratings = new ArrayList<Rating>();
	}

	/**
	 * Calcule le hashCode basé sur l'ID de l'utilisateur.
	 *
	 * @return le hashCode.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	/**
	 * Compare cet objet avec un autre pour l'égalité. Les utilisateurs sont
	 * considérés comme égaux s'ils ont le même ID.
	 *
	 * @param obj l'objet à comparer avec cet utilisateur.
	 * @return true si les objets sont égaux, sinon false.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		return other.id == this.id;
	}
}
