package ch.hevs.businessobject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Représente un menu dans un restaurant. Le menu contient une liste de plats.
 *
 * @author [Arnaud Michelet & Arthur Avez]
 * @version 1.0
 */
@Entity
@Table(name = "Menu")
public class Menu {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "name")
	private String name;

	@ManyToOne
	@JoinColumn(name = "FK_RESTAURANT")
	private Restaurant restaurant;

	@OneToMany(mappedBy = "menu", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Plate> plates;

	/**
	 * Constructeur par défaut, initialise la liste de plats.
	 */
	public Menu() {
		plates = new ArrayList<>();
	}

	/**
	 * Construit un menu avec un nom spécifique et un restaurant.
	 *
	 * @param name       le nom du menu.
	 * @param restaurant le restaurant auquel ce menu appartient.
	 */
	public Menu(String name, Restaurant restaurant) {
		this();
		this.name = name;
		this.restaurant = restaurant;
	}

	/**
	 * Retourne l'ID du menu.
	 *
	 * @return ID du menu.
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Définit l'ID du menu.
	 *
	 * @param id l'ID à définir.
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Retourne le nom du menu.
	 *
	 * @return le nom du menu.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Définit le nom du menu.
	 *
	 * @param name le nom à définir.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Retourne le restaurant auquel ce menu appartient.
	 *
	 * @return le restaurant associé.
	 */
	public Restaurant getRestaurant() {
		return restaurant;
	}

	/**
	 * Définit le restaurant auquel ce menu appartient.
	 *
	 * @param restaurant le restaurant à associer.
	 */
	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	/**
	 * Retourne la liste de plats dans ce menu.
	 *
	 * @return la liste de plats.
	 */
	public List<Plate> getPlates() {
		return plates;
	}

	/**
	 * Définit la liste de plats de ce menu.
	 *
	 * @param plates la liste de plats à définir.
	 */
	public void setPlates(List<Plate> plates) {
		this.plates = plates;
	}

	/**
	 * Ajoute un plat à la liste de plats de ce menu.
	 *
	 * @param plate le plat à ajouter.
	 */
	public void addPlate(Plate plate) {
		plates.add(plate);
	}

	/**
	 * Supprime un plat de la liste de plats de ce menu.
	 *
	 * @param plate le plat à supprimer.
	 */
	public void removePlate(Plate plate) {
		plates.remove(plate);
	}

	/**
	 * Retourne le code de hachage de cet objet Menu, basé sur son ID.
	 * 
	 * @return le code de hachage de cet objet Menu.
	 */
	@Override
	public int hashCode() {
		return id.hashCode();
	}

	/**
	 * Détermine si un autre objet est égal à cet objet Menu. Les menus sont
	 * considérés comme égaux s'ils ont le même ID.
	 *
	 * @param obj l'objet à comparer avec cet objet Menu.
	 * @return vrai si les objets sont égaux, faux sinon.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (!(obj instanceof Menu)) {
			return false;
		}
		Menu other = (Menu) obj;
		return other.id.equals(this.id);
	}
}